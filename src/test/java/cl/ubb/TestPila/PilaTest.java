package cl.ubb.TestPila;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;




public class PilaTest {
	
	public class NumeroPerfectoTest {
		
	
		@Test
		
		public void LaPilaEstaVacia() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado;
			/*act*/
			resultado = p.apilar(n, "null");
			/*assert*/
			assertTrue(resultado);

	}
		@Test
		public void AgregarNumeroUnoYStackNoVacia() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado;
			/*act*/
			resultado = p.apilar(n, "1");
			/*assert*/
			assertFalse(resultado);

	}
		@Test
		public void AgregarNumero1y2YStackNoVacia() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado1;
			boolean resultado2;
			/*act*/
			resultado1 = p.apilar(n, "1");
			resultado2 = p.apilar(n, "2");
			/*assert*/
			assertFalse(resultado1);
			assertFalse(resultado2);

	}
		@Test
		public void AgregarNumero1y2Ytama�oDeStackEs2() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado1;
			boolean resultado2;
			int tama�o;
			/*act*/
			resultado1 = p.apilar(n, "1");
			resultado2 = p.apilar(n, "2");
			tama�o= p.tama�o(n);
			/*assert*/
			assertFalse(resultado1);
			assertFalse(resultado2);
			assertEquals(tama�o, 2);

	}
		@Test
		public void AgregarNumero1yHacerPopDevuelve1() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado1;
			String numero;
			/*act*/
			resultado1 = p.apilar(n, "1");
			numero = p.pop(n);
			/*assert*/
			assertFalse(resultado1);
			assertEquals(numero,"1");
	

	}
		@Test
		public void AgregarNumero1y2yHacerPopDevuelve2() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado1;
			boolean resultado2;
			String numero;
			/*act*/
			resultado1 = p.apilar(n, "1");
			resultado2 = p.apilar(n, "2");
			numero = p.pop(n);
			/*assert*/
			assertFalse(resultado1);
			assertFalse(resultado2);
			assertEquals(numero,"2");
	

	}
		@Test
		public void AgregarNumero3y4yHacerPopDevuelve4y3() {
			/*arrange*/
			Pila p = new Pila();
			String n[] = null;
			boolean resultado1;
			boolean resultado2;
			String numero1;
			String numero2;
			/*act*/
			resultado1 = p.apilar(n, "3");
			resultado2 = p.apilar(n, "4");
			numero1 = p.pop(n);
			numero2 = p.pop(n);
			
			/*assert*/
			assertFalse(resultado1);
			assertFalse(resultado2);
			assertEquals(numero1,"4");
			assertEquals(numero2,"3");
	

	}
	}
}



